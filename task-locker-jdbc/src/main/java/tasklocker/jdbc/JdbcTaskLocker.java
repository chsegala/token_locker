package tasklocker.jdbc;

import org.springframework.jdbc.core.JdbcTemplate;
import tasklocker.TaskLocker;

import javax.sql.DataSource;
import java.util.Date;
import java.util.List;

/**
 * A {@link TaskLocker} implementation using any common RDBS to communicate
 * between instances, locking using a table.
 */
public class JdbcTaskLocker implements TaskLocker {
    private final DataSource dataSource;
    private final String group;
    private final JdbcTemplate template;
    private final long maxDeadMillis;

    public JdbcTaskLocker(DataSource dataSource, String groupName, long maxDeadMillis) {
        this.dataSource = dataSource;
        this.template = new JdbcTemplate(dataSource);
        this.group = groupName;
        this.maxDeadMillis = maxDeadMillis;

        createTable();
    }

    @Override
    public String getGroup() {
        return group;
    }

    @Override
    public boolean acquire(String token) {
        List<String> groups = template.queryForList(JdbcUtils.SELECT_SQL, new Object[]{token}, String.class);
        if (groups.contains(getGroup())) {
            template.update(JdbcUtils.UPDATE_TIME, new Date(), token, getGroup());
            return true;
        } else if (groups.size() == 0) {
            template.update(JdbcUtils.INSERT_LOCK, getGroup(), token, new Date(), new Date());
            return true;
        }
        releaseDeadLocks(token);
        return false;
    }

    @Override
    public void release() {
        template.update(JdbcUtils.REALEASE_SQL, getGroup());
    }

    protected void releaseDeadLocks(String token) {
        template.update(JdbcUtils.REALEASE_DEAD_SQL, token, cutoffDate());
    }

    protected void createTable() {
        template.update(JdbcUtils.CREATE_SQL);
    }

    protected Date cutoffDate() {
        return new Date(new Date().getTime() + maxDeadMillis);
    }
}
