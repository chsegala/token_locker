package tasklocker.jdbc;

/**
 * Utility class to check and modify the database schema, providing a way to
 * initialize the database for the use of the {@link JdbcTaskLocker}
 */
public class JdbcUtils {
    protected static String CREATE_SQL = "CREATE TABLE IF NOT EXISTS TASK_LOCKER( " +
            "group_name varchar(100), " +
            "task_name varchar(100), " +
            "created_at varchar(100), " +
            "updated_at varchar(100) " +
            ");";
    public static final String INSERT_LOCK = "INSERT INTO TASK_LOCKER(group_name, task_name, created_at, updated_at) " +
            "VALUES(?, ?, ?, ?)";
    protected static String SELECT_SQL = "SELECT DISTINCT group_name " +
            "FROM TASK_LOCKER " +
            "WHERE task_name = ? ";
    public static final String UPDATE_TIME = "UPDATE TASK_LOCKER SET updated_at = ? WHERE task_name = ? AND group_name = ?";
    protected static final String REALEASE_SQL = "DELETE FROM TASK_LOCKER WHERE group_name = ?";
    protected static final String REALEASE_DEAD_SQL = "DELETE FROM TASK_LOCKER WHERE task_name = ? AND updated_at < ?";
}
