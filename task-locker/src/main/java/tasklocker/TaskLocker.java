package tasklocker;

import java.io.Serializable;

/**
 * Every implementation must allow only one instance of any given task. Implementations must
 * be thread safe and have a common point of communication
 */
public interface TaskLocker extends Serializable {
    String getGroup();

    boolean acquire(String name);
    void release();
}
