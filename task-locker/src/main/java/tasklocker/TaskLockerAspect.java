package tasklocker;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Aspect used to decorate tasks methods, the method won't be executed
 * if it is impossible to acquire the lock.
 */
@Aspect
public class TaskLockerAspect {

    private static Logger log = LoggerFactory.getLogger(TaskLockerAspect.class);

    private final TaskLocker locker;

    public TaskLockerAspect(TaskLocker locker) {
        this.locker = locker;
    }

    @Pointcut("@annotation(annotation)")
    public void withAnnotation(AtomicTask annotation) {
    }

    @Pointcut("execution(* *(..))")
    public void atExecution() {
    }

    @Around("withAnnotation(annotation) && atExecution()")
    public Object aroundAdvice(ProceedingJoinPoint joinPoint, AtomicTask annotation) throws Throwable {
        log.debug("Atomic task trying to start: " + annotation.value());
        if (locker.acquire(annotation.value())) {
            log.debug("Atomic task acquired lock, running: " + annotation.value());
            Object ret = joinPoint.proceed();
            log.debug("Atomic task runned: " + annotation.value());
            return ret;
        }
        log.debug("Atomic task skipped: " + annotation.value());
        return null;
    }
}
