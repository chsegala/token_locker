package tasklocker;

/**
 * Created by chsegala on 5/13/16.
 */
public class LockToken {
    private final String lockName;
    private final String lockGroup;

    public LockToken(String lockName, String lockGroup) {
        this.lockName = lockName;
        this.lockGroup = lockGroup;
    }

    public String getLockName() {
        return lockName;
    }

    public String getLockGroup() {
        return lockGroup;
    }

    @Override
    public int hashCode() {
        if (getLockName() == null || getLockGroup() == null) return 0;
        return getLockName().hashCode() + getLockGroup().hashCode() * 10;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !obj.getClass().equals(getClass())) return false;
        LockToken c = (LockToken) obj;

        if (getLockName() == null || getLockGroup() == null) return false;
        return getLockName().equals(c.getLockName()) && getLockGroup().equals(c.getLockGroup());
    }
}
